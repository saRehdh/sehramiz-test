<?php

return [
    'mens' => 'مردانه',
    'men' => 'مرد',
    'womens' => 'زنانه',
    'women' => 'زن',
    'enter' => 'وارد شده',
    'not_enter' => 'وارد نشده',
    'unkown_gender' => 'نامشخص'
];
