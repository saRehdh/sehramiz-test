<?php

return [
    'create_successfully' => 'با موفقیت ایجاد شد.',
    'update_successfully' => 'با موفقیت ویرایش شد.',
    'delete_successfully' => 'با موفقیت حذف شد.',
    'mail_sent_successfully' => 'ایمیل با موفقیت ارسال شد.',
    'delete-confirm' => 'آیا مطمئن به حذف هستید؟',
    'eanu' => 'خطایی رخ داد، صفحه بروز نشد.',
    'error_exception' => 'متاسفیم، خطایی رخ داده است.',
    'admin-delete-itsefl' => 'شما نمیتوانید خودتان را حذف کنید.',
    'admin-disable-itsefl' => 'شما نمیتوانید خودتان را غیرفعال کنید.',
    'user-charged-successfully' => 'اعتبار کاربر با موافقیت شارژ شد.',
    'user-retreated-successfully' => 'اعتبار کاربر با موفقیت کاهش یافت'
];
