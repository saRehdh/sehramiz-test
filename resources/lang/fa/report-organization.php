<?php

return [
    C::ORGANIZATION_UPDATE_PROFILE."_OP" => 'ویرایش پروفایل',
    C::ORGANIZATION_UPDATE_PROFILE => '',

    C::ORGANIZATION_LOGIN."_OP" => 'ورود',
    C::ORGANIZATION_LOGIN => '',

    C::ORGANIZATION_LOGOUT."_OP" => 'خروج',
    C::ORGANIZATION_LOGOUT => '',

    C::ORGANIZATION_CREATE_USER."_OP" => 'ایجاد کاربر جدید',
    C::ORGANIZATION_CREATE_USER => 'کاربر با آی‌دی :id و نام :name را ایجاد کرد.',

    C::ORGANIZATION_UPDATE_USER."_OP" => 'ویرایش کاربر',
    C::ORGANIZATION_UPDATE_USER => 'کاربر با آی‌دی :id و نام :name را ویرایش کرد.',

    C::ORGANIZATION_DELETE_USER."_OP" => 'حذف کاربر',
    C::ORGANIZATION_DELETE_USER => 'کاربر با آی‌دی :id و نام :name را حذف کرد.',

    C::ORGANIZATION_CREATE_ORGANIZATION_USER_GROUP."_OP" => 'ایجاد گروه کاربری جدید',
    C::ORGANIZATION_CREATE_ORGANIZATION_USER_GROUP => 'گروه کاربری با آی‌دی :id و نام :name را ایجاد کرد.',

    C::ORGANIZATION_UPDATE_ORGANIZATION_USER_GROUP."_OP" => 'ویرایش گروه کاربری',
    C::ORGANIZATION_UPDATE_ORGANIZATION_USER_GROUP => 'گروه کاربری با آی‌دی :id و نام :name را ویرایش کرد.',

    C::ORGANIZATION_DELETE_ORGANIZATION_USER_GROUP."_OP" => 'حذف گروه کاربری',
    C::ORGANIZATION_DELETE_ORGANIZATION_USER_GROUP => 'گروه کاربری با آی‌دی :id و نام :name را حذف کرد.',
];
