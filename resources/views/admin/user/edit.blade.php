@extends('admin.layout.main')

@section('title', 'ویرایش کاربر '.$user->full_name)

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">کاربران</h1>
        <div class="panel panel-default">
            <div class="panel-heading">ویرایش کاربر <b>{{$user->full_name}}</b>
                <a class="btn btn-default btn-xs pull-left" href="{{action('Admin\UserController@getIndex')}}" title="برگشت"><i class="fa fa-reply"></i></a>
            </div>
            <div class="panel-body">
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif
                <form action="{{action('Admin\UserController@postUpdate')}}" method="post">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>موبایل @required</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('mobile', $user->mobile, ['class' => 'form-control en ltr', 'required']) !!}
                            {!! $errors->first('mobile', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>ایمیل</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('email', $user->email, ['class' => 'form-control en ltr']) !!}
                            {!! $errors->first('email', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>نام</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
                            {!! $errors->first('name', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>نام خانوادگی</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('last_name', $user->last_name, ['class' => 'form-control']) !!}
                            {!! $errors->first('last_name', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>جنسیت</label>
                        </div>
                        <div class="col-md-3">
                            {!! Form::select('gender', Helper::gendersList(false), $user->gender, ['class' => 'form-control', 'placeholder' => 'لطفا انتخاب کنید...']) !!}
                        </div>
                        {!! $errors->first('gender', "<p class='text text-danger'>:message</p>") !!}
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>تاریخ تولد</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::text('birth_date', is_null($user->birth_date) ? '' : Date::make($user->birth_date)->toj()->fa('Y/m/d'), ['class' => 'form-control date-picker']) !!}
                            {!! $errors->first('birth_date', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>کد ملی</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('code_meli', $user->code_meli, ['class' => 'form-control en ltr']) !!}
                            {!! $errors->first('code_meli', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>رمز عبور</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::password('password', ['class' => 'form-control en ltr']) !!}
                            {!! $errors->first('password', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                        <div class="col-md-12">
                            <p class="text-warning">اگر مایل به تغییر رمز نیستید، این فیلد را خالی رها کنید.</p>
                            <p class="text-warning">حداقل طول رمز 8 کاراکتر</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>تایید رمز عبور</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::password('password_confirmation', ['class' => 'form-control en ltr']) !!}
                            {!! $errors->first('password_confirmation', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>استان</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::text('state', $user->state, ['class' => 'form-control']) !!}
                            {!! $errors->first('state', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>شهر</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::text('city', $user->city, ['class' => 'form-control']) !!}
                            {!! $errors->first('city', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>وضعیت</label>
                        </div>
                        <div class="col-md-2">
                            <label>غیر فعال</label>
                            {!! Form::radio('status', '0', $user->status == 0 ? true : false, ['class' => 'icheck-status disable']) !!}
                            <label>فعال</label>
                            {!! Form::radio('status', '1', $user->status == 1 ? true : false, ['class' => 'icheck-status enable']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="user_id" value="{{$user->user_id}}">
                        <div class="col-md-6 text-right">
                            <a href="{{action('Admin\UserController@getIndex')}}" class="btn btn-default">برگشت</a>
                        </div>
                        <div class="col-md-6 text-left">
                            <button type="submit" class="btn btn-info"><i class="fa fa-pencil"></i> ذخیره</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
