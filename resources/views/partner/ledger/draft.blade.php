@extends('pool-admin.layout.main')

@section('title', 'مشاهده حواله')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">حسابداری</h1>
        <div class="panel panel-default">
            <div class="panel-heading">مشاهده حواله</div>
            <div class="panel-body">
                <h2 class="text-center">حواله</h2>
                <table class="table clean">
                    <tr>
                        <td>طرف حساب: {{$pool->name}}</td>
                        <td class="text-left">تاریخ حواله: <span dir="ltr">{{@to_jalali($ledger->date, 'Y/m/d H:i:s l')}}</span></td>
                    </tr>
                    <tr>
                        <td>شماره حواله: {{@tr_num($ledger->draft_id, 'fa')}}</td>
                        <td class="text-left">ساعت حواله: {{@to_time($ledger->date, false, true)}}</td>
                    </tr>
                </table>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>ردیف</th>
                            <th>مبلغ حواله</th>
                            <th>توضیحات</th>
                            <th>واریز به بانک</th>
                            <th>صاحب حساب</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="en">1</td>
                            <td><span class="en">{{price($ledger->liability)->sep()}}</span> ریال</td>
                            <td>{{$ledger->description}}</td>
                            <td>{{$ledger->poolAccount->bank_name}}</td>
                            <td>{{$ledger->poolAccount->owner_name}}</td>
                        </tr>
                    </tbody>
                </table>
                <div class="text-left">
                    <a href="{{URL::action('PoolAdmin\LedgerController@getIndex')}}" class="btn btn-default"><i class="fa fa-reply"></i> بازگشت</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
