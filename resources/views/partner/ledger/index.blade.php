@extends('pool-admin.layout.main')

@section('title', 'معین حساب‌ها')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">حسابداری</h1>
        <div class="panel panel-default">
            <div class="panel-heading">معین حساب‌ها</div>
            <div class="panel-body">
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif
                <div class="panel panel-warning">
                    <div class="panel-heading">فیلتر</div>
                    <div class="panel-body">
                        <form action="" method="get">
                            <div class="col-md-6">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label>از تاریخ</label>
                                        {!! Form::text('from', to_j($from->format('Y-m-d'), false), ['class' => 'form-control date-picker']) !!}
                                        {!! $errors->first('from', "<p class='text text-danger'>:message</p>") !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label>تا تاریخ</label>
                                        {!! Form::text('to', to_j($to->format('Y-m-d'), false), ['class' => 'form-control date-picker']) !!}
                                        {!! $errors->first('to', "<p class='text text-danger'>:message</p>") !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-info pull-left"><i class="fa fa-refresh"></i> بروزرسانی</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <h4 class="text-warning">جمع نهایی: <span class="en">{{price(abs($pool->final_ledger))->sep()}}</span> ریال {{Helper::printLedgerType($pool->final_ledger, true)}}</h4>
                <div class="text-left">
                    <?php
                        $requests = Request::all();
                        $requests['print'] = true;
                    ?>
                    <a href="{{action('PoolAdmin\LedgerController@getAllFactor')}}?{{http_build_query(Request::all())}}" class="btn btn-primary" target="_blank">
                        <i class="fa fa-eye"></i> مشاهده تمام فاکتورها در این بازه
                    </a>
                    <a href="{{action('PoolAdmin\LedgerController@getIndex')}}?{{http_build_query($requests)}}" class="btn btn-primary" target="_blank">
                        <i class="fa fa-print"></i> چاپ
                    </a>
                </div>
                <br>
                <table class="table table-bordered table-hover custom-datatable">
                    <thead>
                        <tr>
                            <th>ردیف</th>
                            <th>تاریخ فاکتور</th>
                            <th>شماره فاکتور</th>
                            <th>شماره حواله</th>
                            <th>بدهکاری</th>
                            <th>بستانکاری</th>
                            <th>مانده</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($ledgers as $position => $ledger)
                            <tr class="{{$ledger->isDraft() ? 'active' : ''}}">
                                <td class="en">{{++$position}}</td>
                                <td class="ltr text-right">{{to_jalali($ledger->date, 'Y/m/d H:i:s l')}}</td>
                                <td class="en">
                                    @if ($ledger->isFactor())
                                    <a href="{{URL::action('PoolAdmin\LedgerController@getFactor', $ledger->ledgerId)}}">{{$ledger->factorId}}</a>
                                    @endif
                                </td>
                                <td class="en">
                                    @if ($ledger->isDraft())
                                    <a href="{{URL::action('PoolAdmin\LedgerController@getDraft', $ledger->ledgerId)}}">{{$ledger->draftId}}</a>
                                    @endif
                                </td>
                                <td><span class="en">{{price($ledger->liability)->sep()}}</span> ریال</td>
                                <td><span class="en">{{price($ledger->credit)->sep()}}</span> ریال</td>
                                <td><span class="en">{{price(abs($ledger->remaining))->sep()}}</span> ریال {{$ledger->remainingStatus}}</td>
                            </tr>
                        @endforeach
                        @if ($ledgers->count() > 0)
                        <tr class="success">
                            <td class="en">{{++$position}}</td>
                            <td colspan="3" class="text-left">جمع کل:</td>
                            <td><span class="en">{{price($ledger->totalLiability)->sep()}}</span> ریال</td>
                            <td><span class="en">{{price($ledger->totalCredit)->sep()}}</span> ریال</td>
                            <td><span class="en">{{price(abs($ledger->remaining))->sep()}}</span> ریال {{$ledger->remainingStatus}}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
