@extends('pool-admin.layout.main')

@section('title', 'لیست رزرو شده‌ها')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">رزرو شده‌ها</h1>
        <div class="panel panel-default">
            <div class="panel-heading">لیست رزرو شده‌ها</div>
            <div class="panel-body">
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif
                <div class="panel panel-warning">
                    <div class="panel-heading">فیلتر</div>
                    <div class="panel-body">
                        <form action="" method="get">
                            <div class="col-md-6">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label>رزرو شده از تاریخ</label>
                                        {!! Form::text('from', to_j($from->format('Y-m-d'), false), ['class' => 'form-control date-picker']) !!}
                                        {!! $errors->first('from', "<p class='text text-danger'>:message</p>") !!}
                                    </div>
                                </div>
                                <br>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label>جنسیت</label>
                                        {!! Form::select('gender', Helper::gendersList(), $gender, ['class' => 'form-control', 'required']) !!}
                                        {!! $errors->first('gender', "<p class='text text-danger'>:message</p>") !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label>تا تاریخ</label>
                                        {!! Form::text('to', to_j($to->format('Y-m-d'), false), ['class' => 'form-control date-picker']) !!}
                                        {!! $errors->first('to', "<p class='text text-danger'>:message</p>") !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info pull-left"><i class="fa fa-refresh"></i> بروزرسانی</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="text-left" style="margin-bottom: 5px;">
                    <?php
                        $requests = Request::all();
                        $requests['print'] = true;
                    ?>
                    <a href="{{action('PoolAdmin\ReservationController@getIndex', http_build_query($requests))}}" class="btn btn-primary" target="_blank">
                        <i class="fa fa-print"></i> چاپ
                    </a>
                </div>
                <table class="table table-bordered table-hover datatable">
                    <thead>
                        <tr>
                            <th>کد واچر</th>
                            <th>کاربر</th>
                            <th>سانس</th>
                            <th>تاریخ سانس</th>
                            <th>زمان</th>
                            <th>تعداد بزرگسال</th>
                            <th>تعداد خردسال</th>
                            <th>جنسیت</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($reserves as $r)
                        <tr>
                            <td class="en">{{$r->voucher}}</td>
                            <td>{{$r->user_name}}</td>
                            <td>{{$r->saens_name}}</td>
                            <td>{{to_j($r->start_datetime, false)}}</td>
                            <td>{{TicketHelper::displayTime($r)}}</td>
                            <td class="en">{{$r->count_enter_adult}}</td>
                            <td class="en">{{$r->count_enter_child}}</td>
                            <td><span class="label label-{{Helper::ifGender($r->gender)}}">{{Helper::printGender($r->gender)}}</span></td>
                            <td>
                                <a href="{{action('PoolAdmin\ReservationController@getView', $r->reservation_id)}}" class="btn btn-primary btn-xs" title="مشاهده"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
