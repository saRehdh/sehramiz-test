@extends('pool-admin.layout.main')

@section('title', 'ایجاد اپراتور جدید')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">اپراتورها</h1>
        <div class="panel panel-default">
            <div class="panel-heading">ایجاد اپراتور جدید
                <a class="btn btn-default btn-xs pull-left" href="{{action('PoolAdmin\OperatorController@getIndex')}}" title="برگشت"><i class="fa fa-reply"></i></a>
            </div>
            <div class="panel-body">
                <form action="{{action('PoolAdmin\OperatorController@postCreate')}}" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>نام کاربری</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('username', '', ['class' => 'form-control en ltr', 'required']) !!}
                            {!! $errors->first('username', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>ایمیل</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('email', '', ['class' => 'form-control en ltr', 'required']) !!}
                            {!! $errors->first('email', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>نام</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('name', '', ['class' => 'form-control']) !!}
                            {!! $errors->first('name', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>نام خانوادگی</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('last_name', '', ['class' => 'form-control']) !!}
                            {!! $errors->first('last_name', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>موبایل</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('mobile', '', ['class' => 'form-control en ltr']) !!}
                            {!! $errors->first('mobile', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>آواتار</label>
                        </div>
                        <div class="col-md-5">
                            <input name="avatar" type="file" class="form-control">
                            {!! $errors->first('avatar', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>رمز عبور</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::password('password', ['class' => 'form-control en ltr']) !!}
                            {!! $errors->first('password', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>تایید رمز عبور</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::password('password_confirmation', ['class' => 'form-control en ltr']) !!}
                            {!! $errors->first('password_confirmation', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>وضعیت</label>
                        </div>
                        <div class="col-md-2">
                            <label>غیر فعال</label>
                            {!! Form::radio('status', '0', false, ['class' => 'icheck-status disable']) !!}
                            <label>فعال</label>
                            {!! Form::radio('status', '1', false, ['class' => 'icheck-status enable']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-plus"></i> ایجاد کن</button>
                        <a href="{{action('PoolAdmin\OperatorController@getIndex')}}" class="btn btn-default">برگشت</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
@include('admin.templates.begard')
<script>
    {{Helper::begardLastIndex()}}
    $(function() {
        begard.init();
    });
</script>
@stop
