<?php

use Illuminate\Database\Seeder;

class FestivalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $festivals = [
            array(
                'festival_id' => 1,
                'name' => 'فستیوال بزرگ',
                'codename' => 'festival-1',
                'code_price' => 50000,
                'code_count_max' => null,
                'status' => 1,
            )
        ];
        DB::table('festivals')->insert($festivals);
    }
}
