<?php

Route::group(['prefix' => 'api/v1/general', 'namespace' => 'Api\General'], function() {
    // Route::get('sliders', ['uses' => 'SliderController@getSliders']);
});

Route::group(['middleware' => 'web'], function() {
    Route::get('/', function() {
        return 'Please redirect to auth/admin/login';
    });

    /**
     * Admin Auth
     */
    Route::controller('auth/admin', 'Auth\AdminAuthController');

    /**
     * Admin
     */
    Route::group(['namespace' => 'Admin', 'middleware' => 'auth:admin', 'prefix' => 'admin'], function() {
        Route::get('/', ['as' => 'admin', function() { return redirect(action('Admin\DashboardController@getIndex')); }]);

        Route::controller('dashboard', 'DashboardController');
        Route::controller('admin', 'AdminController');
        Route::controller('festival', 'FestivalController');
    });
});
