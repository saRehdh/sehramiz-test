<?php

namespace Sehramiz\Http\Controllers\Admin;

use DB;
use Auth;
use Illuminate\Http\Request;
use Sehramiz\Models\Partner;
use Sehramiz\Models\Festival;
use Illuminate\Support\Facades\Lang;
use Sehramiz\Resources\Festival\FestivalValidator;
use Sehramiz\Resources\Festival\FestivalRepository;

class FestivalController extends Controller
{
    protected $category = 'festival';

    public function __construct(Request $request, FestivalRepository $festivalRepository, FestivalValidator $festivalValidator)
    {
        $this->festivalRepository = $festivalRepository;
        $this->festivalValidator = $festivalValidator;
        $this->request = $request;
    }

    public function getIndex()
    {
        return $this->fire('admin.festival.index', array(
            'festivals' => Festival::all()
        ));
    }

    public function getCreate()
    {
        return $this->fire('admin.festival.create', array(
            'partners' => Partner::all(['name', 'partner_id'])->lists('full_name', 'partner_id')
        ));
    }

    public function postCreate()
    {
        if (($errors = $this->festivalValidator->validate()) !== true)
            return redirect()->back()->withInput()->withErrors($errors);

        try {
            $festival = $this->festivalRepository->newFestival();
            $festivalPartners = $this->festivalRepository->partners();

            $festival = $this->festivalRepository->transaction($festival, $festivalPartners);

        } catch(\Exception $e) {
            return redirect(action('Admin\FestivalController@getIndex'))
                ->with('f-message', ['t' => 'danger', 'm' => Lang::get('admin-messages.error_exception', array('message' => $e->getMessage()))]);
        }

        return redirect(action('Admin\FestivalController@getIndex'))
            ->with('f-message', ['t' => 'success', 'm' => Lang::get('admin-messages.create_successfully')]);
    }

    public function getUpdate($festivalId = null)
    {
        try {
            $festival = Festival::findOrFail($festivalId);

        } catch(\Exception $e) {
            return redirect(action('Admin\FestivalController@getIndex'))
                ->with('f-message', ['t' => 'danger', 'm' => Lang::get('admin-messages.error_exception', array('message' => $e->getMessage()))]);
        }

        return $this->fire('admin.festival.edit', array(
            'festival' => $festival,
            'partners' => Partner::all(['name', 'partner_id'])->lists('full_name', 'partner_id'),
            'selectedPartners' => DB::table('festivals_partners')->where('festival_id', $festival->festival_id)->pluck('partner_id')
        ));
    }

    public function postUpdate()
    {
        $festivalId = $this->request->get('festival_id');

        if (($errors = $this->festivalValidator->validate(true)) !== true)
            return redirect()->back()->withInput()->withErrors($errors);

        try {
            $festival = $this->festivalRepository->update($festivalId);
            $festivalPartners = $this->festivalRepository->partners();

            $this->festivalRepository->transaction($festival, $festivalPartners);
        } catch(\Exception $e) {
            return redirect(action('Admin\FestivalController@getIndex'))
                ->with('f-message', ['t' => 'danger', 'm' => Lang::get('admin-messages.error_exception', array('message' => $e->getMessage()))]);
        }

        return redirect(action('Admin\FestivalController@getIndex'))
            ->with('f-message', ['t' => 'success', 'm' => Lang::get('admin-messages.update_successfully')]);
    }

    public function getDestroy($festivalId = null)
    {
        try {
            $festival = Festival::findOrFail($festivalId);

            $festival->delete();
        } catch(\Exception $e) {
            return redirect(action('Admin\FestivalController@getIndex'))
                ->with('f-message', ['t' => 'danger', 'm' => Lang::get('admin-messages.error_exception', array('message' => $e->getMessage()))]);
        }

        return redirect(action('Admin\FestivalController@getIndex'))
            ->with('f-message', ['t' => 'success', 'm' => Lang::get('admin-messages.delete_successfully')]);
    }
}
