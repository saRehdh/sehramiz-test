<?php

namespace Sehramiz\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                switch ($guard) {
                    case 'admin':
                        return redirect()->guest('/');
                        break;

                    case 'pool-admin':
                        return redirect()->guest(action('Auth\PoolAdminAuthController@getLogin'));
                        break;

                    case 'operator':
                        return redirect()->guest(action('Auth\OperatorAuthController@getLogin'));
                        break;

                    case 'front':
                        return redirect()->guest('/');
                        break;

                    case 'organization':
                        return redirect()->guest(action('Auth\OrganizationAuthController@getLogin'));
                        break;
                }
            }
        } else {
            if(!Auth::guard($guard)->user()->status) {
                Auth::guard($guard)->logout();

                switch ($guard) {
                    case 'admin':
                        return redirect()->action('Auth\AdminAuthController@getLogin')
                            ->withErrors(['status' => \Lang::get('auth.admin-status')]);
                        break;

                    case 'pool-admin':
                        return redirect()->action('Auth\PoolAdminAuthController@getLogin')
                            ->withErrors(['status' => \Lang::get('auth.pool-admin-status')]);
                        break;

                    case 'operator':
                        return redirect()->action('Auth\OperatorAuthController@getLogin')
                            ->withErrors(['status' => \Lang::get('auth.operator-status')]);
                        break;

                    case 'front':
                        return redirect()->action('Auth\FrontAuthController@getLogin')
                            ->withErrors(['status' => \Lang::get('auth.front-status')]);
                        break;

                    case 'organization':
                        return redirect()->action('Auth\OrganizationAuthController@getLogin')
                            ->withErrors(['status' => \Lang::get('auth.organization-status')]);
                        break;
                }

            }
        }

        Auth::shouldUse($guard);
        return $next($request);
    }
}
