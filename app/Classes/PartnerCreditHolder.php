<?php

namespace Sehramiz\Classes;

use C;
use Date\Date;
use Sehramiz\Models\Partner;
use Sehramiz\Models\PartnerCreditEvent;
use Sehramiz\Exceptions\NotEnoughCreditException;

class PartnerCreditHolder
{
    /**
     * Once partner model is set, we set it as static variable
     * So we can use it for new instances
     */
    private static $partnerModel = null;

    protected static $handlers = [
        C::PC_E_INCREASE_ADMIN =>         \Sehramiz\Classes\PartnerCreditEvent\Admin::class,
        C::PC_E_DECREASE_ADMIN =>         \Sehramiz\Classes\PartnerCreditEvent\Admin::class,
        C::PC_E_DECREASE_GENERATE_CODE => \Sehramiz\Classes\PartnerCreditEvent\FestivalCode::class,
        C::PC_E_SEND_SMS =>               \Sehramiz\Classes\PartnerCreditEvent\Festival::class,
    ];

    /**
     * @var int
     */
    protected $partnerId;

    /**
     * @var \Sehramiz\Models\Partner
     */
    protected $partner;

    /**
     * @var \Sehramiz\Models\PartnerCreditEvent
     */
    protected $event;

    /**
     * @param int $partnerId
     * @param null|\Sehramiz\Models\Partner $partner
     */
    public function __construct($partnerId, Partner $partner = null)
    {
        $this->partnerId = $partnerId;
        $this->setPartnerModel($partner);
    }

    /**
     * @param null|\Sehramiz\Models\Partner $partner
     * @return void
     */
    protected function setPartnerModel(Partner $partner = null)
    {
        if (is_null($partner)) {
            if (!is_null(self::$partnerModel) && self::$partnerModel->partner_id === $this->partnerId) {
                $this->partner = self::$partnerModel;
            } else {
                $this->partner = Partner::find($this->partnerId);
                self::$partnerModel = $this->partner;
            }
        } else {
            $this->partner = $partner;
            self::$partnerModel = $partner;
        }
    }

    /**
     * Increase partner credit
     *
     * @return int
     */
    public function increase($price, $code, $relatedId, $description = null)
    {
        $this->partner->credit +=  $price;
        $this->partner->save();

        $partnerCreditEvent = new PartnerCreditEvent;
        $partnerCreditEvent->code = $code;
        $partnerCreditEvent->price = $price;
        $partnerCreditEvent->payment = C::PAID;
        $partnerCreditEvent->partner_id = $this->partnerId;
        $partnerCreditEvent->related_id = $relatedId;
        $partnerCreditEvent->description = $description;
        $partnerCreditEvent->save();

        return $partnerCreditEvent->partner_credit_event_id;
    }

    /**
     * Decrease partner credit
     */
    public function decrease($price, $code, $relatedId, $description = null)
    {
        if (!$this->hasCredit($price)) return false;

        $this->partner->credit -=  $price;
        $this->partner->save();

        $partnerCreditEvent = new PartnerCreditEvent;
        $partnerCreditEvent->code = $code;
        $partnerCreditEvent->price = -1 * $price;
        $partnerCreditEvent->payment = C::PAID;
        $partnerCreditEvent->partner_id = $this->partnerId;
        $partnerCreditEvent->related_id = $relatedId;
        $partnerCreditEvent->description = $description;
        $partnerCreditEvent->save();

        return true;
    }

    /**
     * Decrease partner credit
     */
    public function forceDecrease($price, $code, $relatedId, $description = null)
    {
        $this->partner->credit -=  $price;
        $this->partner->save();

        $partnerCreditEvent = new PartnerCreditEvent;
        $partnerCreditEvent->code = $code;
        $partnerCreditEvent->price = -1 * $price;
        $partnerCreditEvent->payment = C::PAID;
        $partnerCreditEvent->partner_id = $this->partnerId;
        $partnerCreditEvent->related_id = $relatedId;
        $partnerCreditEvent->description = $description;
        $partnerCreditEvent->save();

        return true;
    }

    public function decreaseBunch(array $events)
    {
        $price = array_sum(array_column($events, 'price'));
        if (!$this->hasMinimumCredit($price))
            throw new NotEnoughCreditException;

        $this->partner->credit -= $price;
        $this->partner->save();

        foreach ($events as &$event) {
            $event['partner_id'] = $this->partnerId;
            $event['payment'] = C::PAID;
            $event['created_at'] = Date::now();
            $event['updated_at'] = Date::now();
        }

        PartnerCreditEvent::insert($events);

        return true;
    }

    /**
     * @param int $price
     * @return bool
     */
    public function hasCredit($price)
    {
        return $this->partner->credit >= $price;
    }

    /**
     * @param int $price
     * @return bool
     */
    public function hasMinimumCredit($price)
    {
        return (abs($this->partner->minimum_credit) + $this->partner->credit) >= $price;
    }

    /**
     * @return int
     */
    public function credit()
    {
        return $this->partner->credit;
    }

    /**
     * @param \Sehramiz\Models\PartnerCreditEvent
     */
    public function setEvent(PartnerCreditEvent $event)
    {
        $this->event = $event;
    }

    /**
     * @return int
     */
    public function event()
    {
        return new self::$handlers[$this->event->code]($this->partner, $this->event->code, $this->event);
    }

    /**
     * Return credit event property model instead of this class property
     *
     * @param string $property
     */
    public function __get($property)
    {
        return $this->event->$property;
    }

}
