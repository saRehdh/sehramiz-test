<?php

namespace Sehramiz\Classes\PartnerCreditEvent;

use Sehramiz\Models\FestivalCode as FestivalCodeModel;
use Sehramiz\Models\Festival as FestivalModel;

class FestivalCode extends BaseEvent
{
    protected function loadRelated()
    {
        $this->festivalCode = FestivalCodeModel::find($this->event->related_id);
        $this->festival = FestivalModel::find($this->festivalCode->festival_id);
    }

    public function festivalName()
    {
        return $this->festival->name;
    }

    public function festivalId()
    {
        return $this->festivalCode->festival_id;
    }
}
