<?php

namespace Sehramiz\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerIpRestrict extends Model
{
    protected $table = 'partners_ip_restrict';

    protected $primaryKey = 'partners_ip_restrict';

    public $timestamps = false;
}
